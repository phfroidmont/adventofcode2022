import scala.io.Source

object Day1 extends App:

  val calories = Source
    .fromURL(getClass.getResource("day1Input.txt"))
    .mkString
    .split('\n')
    .toList

  val elvesCalories = calories
    .foldLeft(List(0)) {
      case (elvesTotal, caloriesValue) if caloriesValue.isEmpty =>
        0 :: elvesTotal
      case (elfTotal :: elveTotal, caloriesValue) =>
        caloriesValue.toInt + elfTotal :: elveTotal
    }

  println(s"Part 1: ${elvesCalories.max}")

  val top3ElvesCalories = elvesCalories.sorted.takeRight(3).sum

  println(s"Part 2: ${top3ElvesCalories}")

import scala.io.Source

object Day4 extends App:

  val input = Source
    .fromURL(getClass.getResource("day4Input.txt"))
    .mkString
    .split('\n')
    .toList

  def parsePair(string: String): (List[Int], List[Int]) =
    string.split(',').map(parseRange) match
      case Array(elf1, elf2) => (elf1, elf2)

  def parseRange(string: String): List[Int] =
    string.split('-').map(_.toInt) match
      case Array(from, to) => List.range(from, to + 1)

  val part1 = input
    .map(parsePair)
    .count(pair => pair._1.containsSlice(pair._2) || pair._2.containsSlice(pair._1))

  println(s"Part 1: $part1")

  val part2 = input
    .map(parsePair)
    .count(pair => pair._1.intersect(pair._2).size > 0)

  println(s"Part 2: $part2")

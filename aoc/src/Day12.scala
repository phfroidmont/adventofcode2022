import scala.io.Source

object Day12 extends App:

  val input = Source
    .fromURL(getClass.getResource("day12Input.txt"))
    .mkString
    .split('\n')

  val width  = input.head.length
  val height = input.length

  type Point = (Int, Int)
  type Grid  = Map[Point, Char]
  type Path  = List[Point]

  val grid: Grid =
    (for
      (row, y)  <- input.zipWithIndex
      (cost, x) <- row.zipWithIndex
    yield (x, y) -> cost).toMap

  extension (point: Point)
    def x = point._1
    def y = point._2
    def neighbours: Path = List(
      (x - 1, y),
      (x + 1, y),
      (x, y - 1),
      (x, y + 1)
    )
    def estimateCost(to: Point): Int = (x - to.x).abs + (y - to.y).abs

  extension (grid: Grid)
    def getCost(current: Point, neighbour: Point) =
      if grid(neighbour) - grid(current) > 1 then 100000
      else 1

    def getNeighbours(point: Point): List[Point] = point.neighbours.filter(grid.contains)

    def searchShortestPath(start: Set[Point], finish: Point): Option[Path] =
      case class PointInfo(costFromStart: Int, estimatedTotalCost: Int, shortestPath: Path)

      def recur(closed: Set[Point], open: Map[Point, PointInfo]): Option[Path] =
        val (current, PointInfo(currentCostFromStart, estimatedTotalCost, shortestPath)) =
          open.minBy(_._2.estimatedTotalCost)

        if open.isEmpty then None
        else if current == finish then Some(shortestPath)
        else
          val openPoints = grid
            .getNeighbours(current)
            .filterNot(closed.contains)
            .foldLeft(open - current) { case (open, neighbour) =>
              val neighborCostFromStart = currentCostFromStart + grid.getCost(current, neighbour)
              if open.get(neighbour).exists(_.costFromStart <= neighborCostFromStart) then open
              else
                open.updated(
                  neighbour,
                  PointInfo(
                    neighborCostFromStart,
                    neighborCostFromStart + neighbour.estimateCost(finish),
                    shortestPath.appended(neighbour)
                  )
                )
            }
          recur(closed + current, openPoints)
      recur(
        Set(),
        start.toList.map(it => it -> PointInfo(0, it.estimateCost(finish), List(it))).toMap
      )
    end searchShortestPath
  end extension

  val start = Set(grid.toList.filter(_._2 == 'S').head._1)
  val end   = grid.toList.filter(_._2 == 'E').head._1

  val gridWithElevationOnly = grid
    .mapValues(it => if it == 'S' then 'a' else if it == 'E' then 'z' else it)
    .toMap

  val part1 = gridWithElevationOnly.searchShortestPath(start, end).map(_.length - 1)
  println("Part 1:")
  println(part1)

  val start2 = gridWithElevationOnly.filter(_._2 == 'a').keySet

  val part2 = gridWithElevationOnly.searchShortestPath(start2, end).map(_.length - 1)
  println("Part 2:")
  println(part2)
end Day12

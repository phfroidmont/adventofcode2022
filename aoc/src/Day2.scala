import scala.io.Source

object Day2 extends App:

  val input = Source
    .fromURL(getClass.getResource("day2Input.txt"))
    .mkString
    .split('\n')
    .toList

  enum Move:
    case Rock, Paper, Scissors

  object Move:
    val parse: Char => Move =
      case 'A' => Rock
      case 'X' => Rock
      case 'B' => Paper
      case 'Y' => Paper
      case 'C' => Scissors
      case 'Z' => Scissors

    val loosingCounterpart: Move => Move =
      case Rock     => Scissors
      case Paper    => Rock
      case Scissors => Paper

    val winningCounterpart: Move => Move =
      case Rock     => Paper
      case Paper    => Scissors
      case Scissors => Rock

    val points: Move => Int =
      case Rock     => 1
      case Paper    => 2
      case Scissors => 3

  enum MatchResult:
    case Win, Loss, Draw

  object MatchResult:
    val parse: Char => MatchResult =
      case 'X' => Loss
      case 'Y' => Draw
      case 'Z' => Win

  object Part1:
    val guide = input
      .map(game => Game(Move.parse(game.head), Move.parse(game.last)))

    import Move.*
    import MatchResult.*

    final case class Game(opponent: Move, self: Move):
      val result =
        if self == opponent then Draw
        else if Move.loosingCounterpart(self) == opponent then Win
        else Loss

      val points = result match
        case Win  => 6 + Move.points(self)
        case Loss => Move.points(self)
        case Draw => 3 + Move.points(self)

    val totalScore = guide.map(_.points).sum

  println(s"Part 1: ${Part1.totalScore}")

  object Part2:
    val guide = input
      .map(game => Game(Move.parse(game.head), MatchResult.parse(game.last)))

    import Move.*
    import MatchResult.*

    final case class Game(opponent: Move, targetResult: MatchResult):

      val self =
        if targetResult == Draw then opponent
        else if targetResult == Win then Move.winningCounterpart(opponent)
        else Move.loosingCounterpart(opponent)

      val points = targetResult match
        case Win  => 6 + Move.points(self)
        case Loss => Move.points(self)
        case Draw => 3 + Move.points(self)

    val totalScore = guide.map(_.points).sum

  println(s"Part 2: ${Part2.totalScore}")
end Day2

import scala.io.Source

object Day11 extends App:

  val input = Source
    .fromURL(getClass.getResource("day11Input.txt"))
    .mkString
    .split('\n')
    .toList

  final case class Monkey(
    operation: Long => Long,
    testDiv: Long,
    targetTrue: Int,
    targetFalse: Int,
    items: List[Long],
    inspected: Int = 0)

  val monkeys = input
    .grouped(7)
    .toList
    .map(_.filter(_.nonEmpty))
    .map(lines =>
      Monkey(
        lines(2).split(" ").takeRight(3) match
          case Array("old", "*", "old") => old => old * old
          case Array("old", "+", "old") => old => old + old
          case Array("old", "+", value) => _ + value.toInt
          case Array("old", "*", value) => _ * value.toInt
        ,
        lines(3).split(" ").last.toInt,
        lines(4).split(" ").last.toInt,
        lines(5).split(" ").last.toInt,
        lines(1).split(": ").last.split(", ").map(_.toInt).toList.map(_.toLong)
      )
    ).toList

  val part1 = (0 until (monkeys.length * 20))
    .map(_ % (monkeys.length))
    .foldLeft(monkeys)((monkeys, i) =>
      val m = monkeys(i)
      m.items
        .foldLeft(monkeys) { case (monkeys, item) =>
          val newItem  = m.operation(item)
          val receiver = if (newItem / 3) % m.testDiv == 0 then m.targetTrue else m.targetFalse
          val rm       = monkeys(receiver)
          monkeys.updated(receiver, rm.copy(items = rm.items.appended(newItem / 3)))
        }
        .updated(i, m.copy(items = List(), inspected = m.inspected + m.items.length))
    )
    .map(_.inspected)
    .sorted
    .takeRight(2)
    .product
  println("Part 1:")
  println(part1)

  val modulus = monkeys.map(_.testDiv).product

  val part2 = (0 until (monkeys.length * 10000))
    .map(_ % (monkeys.length))
    .foldLeft(monkeys)((monkeys, i) =>
      val m = monkeys(i)
      m.items
        .foldLeft(monkeys) { case (monkeys, item) =>
          val newItem  = m.operation(item) % modulus
          val receiver = if newItem % m.testDiv == 0 then m.targetTrue else m.targetFalse
          val rm       = monkeys(receiver)

          monkeys.updated(receiver, rm.copy(items = rm.items.appended(newItem)))
        }
        .updated(i, m.copy(items = List(), inspected = m.inspected + m.items.length))
    )
    .map(_.inspected)
    .sorted
    .takeRight(2)
    .map(_.toLong)
    .product
  println("Part 2:")
  println(part2)
end Day11

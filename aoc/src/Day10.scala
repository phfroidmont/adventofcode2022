import scala.io.Source

object Day10 extends App:

  val input = Source
    .fromURL(getClass.getResource("day10Input.txt"))
    .mkString
    .split('\n')
    .map(_.split(" "))
    .flatMap {
      case Array("addx", value) => List(0, value.toInt)
      case Array("noop")        => List(0)
    }

  val registerStates = input.scan(1)(_ + _)

  val part1 = (20 to 220 by 40).map(s => registerStates(s - 1) * s).sum
  println(s"Part 1: $part1")

  val part2 = registerStates
    .grouped(40)
    .map(
      _.zip(0 to 39)
        .map((reg, cycle) => Math.abs(reg - cycle) <= 1)
        .map(if _ then "#" else ".")
        .mkString
    )
    .mkString("\n")
  println("Part 2:")
  println(part2)

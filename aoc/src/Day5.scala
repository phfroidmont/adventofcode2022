import scala.io.Source
import scala.collection.mutable.Stack

object Day5 extends App:

  val input = Source
    .fromURL(getClass.getResource("day5Input.txt"))
    .mkString
    .split('\n')
    .toList

  val crates = input
    .takeWhile(_ != "")
    .dropRight(1)
    .map(_.grouped(4).toList)
    .transpose
    .map(_.flatMap(_.find(_.isLetter)))
    .toArray

  val moves = input
    .dropWhile(_ != "")
    .tail
    .map(_.split(" ").flatMap(_.toIntOption))
    .map { case Array(count, from, to) => (count, from - 1, to - 1) }

  val part1 = moves
    .foldLeft(crates) { case (crates, (count, from, to)) =>
      val movedCrates = crates(from).take(count)
      crates
        .updated(from, crates(from).drop(count))
        .updated(to, crates(to).prependedAll(movedCrates.reverse))
    }.toList
    .map(_.head)
    .mkString

  println(s"Part 1: $part1")

  val part2 = moves
    .foldLeft(crates) { case (crates, (count, from, to)) =>
      val movedCrates = crates(from).take(count)
      crates
        .updated(from, crates(from).drop(count))
        .updated(to, crates(to).prependedAll(movedCrates))
    }.toList
    .map(_.head)
    .mkString

  println(s"Part 2: $part2")
end Day5

import scala.io.Source

object Day3 extends App:

  val input = Source
    .fromURL(getClass.getResource("day3Input.txt"))
    .mkString
    .split('\n')
    .toList

  val part1 = input
    .map(rucksack => rucksack.splitAt(rucksack.length / 2))
    .map(rucksack => rucksack._1.toSet.intersect(rucksack._2.toSet))
    .map(_.map(computePriority).sum)
    .sum

  def computePriority(char: Char): Int =
    if char.isUpper then char - 38
    else char - 96

  println(s"Part 1: $part1")

  val part2 = input
    .map(_.toSet)
    .grouped(3)
    .map(_.reduce(_.intersect(_)))
    .map(_.map(computePriority).sum)
    .sum

  println(s"Part 2: $part2")

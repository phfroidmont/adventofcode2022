import scala.io.Source

object Day6 extends App:

  val input = Source
    .fromURL(getClass.getResource("day6Input.txt"))
    .mkString
    .split('\n')
    .toList

  val part1 = input.head.sliding(4).indexWhere(_.distinct.length == 4) + 4

  println(s"Part 1: $part1")

  val part2 = input.head.sliding(14).indexWhere(_.distinct.length == 14) + 14

  println(s"Part 2: $part2")

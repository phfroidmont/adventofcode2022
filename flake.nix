{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        millWithFixedJdk = pkgs.mill.override { jre = pkgs.jdk; };
      in
      {
        devShell = pkgs.mkShell {
          buildInputs = [ millWithFixedJdk ];
          shellHook = ''
            set -a
            JAVA_HOME=${pkgs.jdk}
            set +a
          '';
        };
      }
    );
}
